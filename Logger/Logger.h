#pragma once

#ifdef LOGGER_EXPORTS  
#define LOGGER_API __declspec(dllexport)   
#else  
#define LOGGER_API __declspec(dllimport)   
#endif 

#include <fstream>
#include <string>

class LOGGER_API Logger
{
public:
	// create the file where the messages will be printed
	Logger(const std::string &fileName);
	~Logger() = default;

	// print something in the file
	template<typename T>
	void Log(const T &printMe)
	{
		logFile << printMe << "\n";
	}
private:
	std::ofstream logFile;
};