#include "stdafx.h"
#include "CppUnitTest.h"

#include "GameManager.h"
#include "Player.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QwirkleTests
{
	TEST_CLASS(GameManagerTests)
	{
	public:

		TEST_METHOD(Constructor)
		{
			std::vector<Player> players;
			std::string name;
			name.assign("Edward");
			Player newPlayer(name);

			players.push_back(newPlayer);

			GameManager game(players);

			Assert::IsTrue(players[0].getPlayerName() == "Kevin");
			Assert::IsTrue(game.getUsedPieces() == 0);
			Assert::IsTrue(game.currentState == GameManager::GameState::STOPPED);

		}

		TEST_METHOD(Pause)
		{

			std::vector<Player> players;
			std::string name;
			name.assign("Maya");
			Player newPlayer(name);

			players.push_back(newPlayer);

			GameManager game(players);
			game.start();

			Assert::IsTrue(game.currentState == GameManager::GameState::RUNNING);

		}

		TEST_METHOD(Stop)
		{

			std::vector<Player> players;
			std::string name;
			name.assign("Edward");
			Player newPlayer(name);

			players.push_back(newPlayer);

			GameManager game(players);
			game.pause();

			Assert::IsTrue(game.currentState == GameManager::GameState::STOPPED);

		}
	};
}