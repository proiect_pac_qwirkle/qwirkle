#include "stdafx.h"
#include "CppUnitTest.h"

#include "Board.h"
#include "Piece.h"
#include "Position.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QwirkleTests
{
	TEST_CLASS(BoardTests)
	{
	public:

		TEST_METHOD(Constructor)
		{
			Board board;
			Assert::IsTrue(board.GetBoard().size() == 1);
			Assert::IsTrue(board.GetBoard()[0].size() == 1);
			Assert::IsTrue(board.GetBoard()[0][0].GetColor() == Piece::Color::None && board.GetBoard()[0][0].GetShape() == Piece::Shape::None);
		}

		TEST_METHOD(PlacingPiece)
		{
			Board board;
			Piece piece(Piece::Color::Green, Piece::Shape::Circle);
			Position position;
			board.PlacePiece(piece, position);
			Assert::IsTrue(board.GetBoard().size() == 3);
			Assert::IsTrue(board.GetBoard()[0].size() == 3);
		}

	};
}
