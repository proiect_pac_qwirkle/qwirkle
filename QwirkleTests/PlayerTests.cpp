#include "stdafx.h"
#include "CppUnitTest.h"

#include "Player.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QwirkleTests
{
	TEST_CLASS(PlayerTests)
	{
	public:

		TEST_METHOD(ConstructorWithParameters)
		{
			Player player("Kevin");

			Assert::IsTrue("Kevin" == player.getPlayerName());
			Assert::IsTrue(Player::PlayerState::STANDBY == player.getPlayerState());

		}

		TEST_METHOD(PlayerStateSetter)
		{
			Player player;

			//player.setPlayerState("WON");

			//Assert::IsTrue("WON" == player.getPlayerState());

		}

		TEST_METHOD(PlayerScoreSetter)
		{
			Player player;

			player.updateScore(1);

			Assert::IsTrue(1 == player.score);

		}

		TEST_METHOD(PLayerNameGetter)
		{
			Player player("John");

			Assert::IsTrue("John" == player.getPlayerName());

		}

	};
}
