#include "stdafx.h"
#include "CppUnitTest.h"

#include "Piece.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QwirkleTests
{
	TEST_CLASS(PieceTests)
	{
	public:

		TEST_METHOD(DefaultConstructor)
		{

			Piece piece(Piece::Color::Red, Piece::Shape::Circle);

			Assert::IsTrue(Piece::Color::Red == piece.GetColor());
			Assert::IsTrue(Piece::Shape::Circle == piece.GetShape());

		}

		TEST_METHOD(ConstructorWithParameters)
		{

			Piece piece(Piece::Color::Blue, Piece::Shape::Diamond);

			Assert::IsTrue(Piece::Color::Blue == piece.GetColor());
			Assert::IsTrue(Piece::Shape::Diamond == piece.GetShape());

		}

		TEST_METHOD(MoveAssignmentOperator)
		{

			Piece piece(Piece::Color::Yellow, Piece::Shape::Star), otherPiece;

			otherPiece = std::move(piece);

			Assert::IsTrue(otherPiece.GetColor() == Piece::Color::Yellow);
			Assert::IsTrue(otherPiece.GetShape() == Piece::Shape::Star);


		}


	};
}