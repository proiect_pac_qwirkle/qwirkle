#include "stdafx.h"
#include "CppUnitTest.h"

#include <sstream>
#include <string>

#include "Board.h"
#include "Piece.h"
#include "Position.h"
#include "DrawableBoard.h"
#include "DrawablePiece.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QwirkleTests
{
	TEST_CLASS(DrawableTests)
	{
	public:

		TEST_METHOD(DrawablePieceConstructor) 
		{
			Piece piece;
			DrawablePiece drawablePiece(piece);
			Assert::IsTrue(drawablePiece.GetPiecePointer() != nullptr);
		}

		TEST_METHOD(DrawableBoardConstructor)
		{
			Board board;
			DrawableBoard drawableBoard(board);
			Assert::IsTrue(drawableBoard.GetBoardPointer() != nullptr);
		}

		TEST_METHOD(PiecePrinting)
		{
			std::stringstream ss;
			Piece piece;
			DrawablePiece drawablePiece(piece);
			ss << drawablePiece;
			Assert::IsTrue(ss.str() == "--");
		}
	};
}
