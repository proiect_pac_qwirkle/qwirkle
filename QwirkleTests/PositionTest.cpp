#include "stdafx.h"
#include "CppUnitTest.h"

#include "Position.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QwirkleTests
{
	TEST_CLASS(PositionTest)
	{
	public:

		TEST_METHOD(Constructor)
		{

			Position position;

			Assert::IsTrue(position.x == 0);
			Assert::IsTrue(position.y == 0);

		}
	};
}
