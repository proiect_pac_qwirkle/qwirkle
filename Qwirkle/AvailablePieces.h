#pragma once
#include "Piece.h"


#include<unordered_map>

class AvailablePieces
{
	//global to see in many functions
	const int numberOfPieces = 108;

public:
	AvailablePieces();

	double RandomFunction();
	void GeneratePieces();		//function that generate random pieces
	void EmplaceCurrentPiece(Piece&& Piece);	//function that put a pice in the bag
	void SwapPiecesFromHand(Piece&& piece);

	friend std::ostream& operator << (std::ostream& os, const AvailablePieces& availablePieces);

private:
	std::vector<Piece> m_bag;
};

