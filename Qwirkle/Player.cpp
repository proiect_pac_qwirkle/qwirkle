#include "Player.h"


Player::Player(){
	// initializes the player's state to 'STANDBY'
	playerStatus = PlayerState::STANDBY;
}

Player::Player(const std::string& name) {
	// sets the player's name and initializes the player's state to 'STANDBY'
	this->name = name;
	playerStatus = PlayerState::STANDBY;
}


Player::~Player(){

}

Player::PlayerState Player::getPlayerState() {
	return playerStatus;
}

void Player::setPlayerState(PlayerState value) {
	playerStatus = value;
}

std::string Player::getPlayerName() {
	return this->name;
}

void Player::setPlayerName(const std::string& name) {
	this->name = name;
}

void Player::updateScore(const int toAdd) {
	// updates the player's current score
	this->score += toAdd;
}
