#include "Board.h"

Board::Board()
{
	m_board.resize(1);
	m_board[0].resize(1);
}

const std::vector<std::vector<Piece>>& Board::GetBoard()
{
	return m_board;
}

void Board::PlacePiece(Piece &piece, const Position &position)
{
	try
	{
		if (position.y >= m_board.size())
			throw "Line doesn't exist.";
		if (position.x >= m_board[position.y].size())
			throw "Column doesn't exist.";
		m_board[position.y][position.x] = std::move(piece);
		if (position.x == m_board[0].size() - 1)
			for (auto &line : m_board)
				line.insert(line.rbegin().base(), Piece());
		if (position.y == m_board.size() - 1)
		{
			std::vector<Piece> toInsert;
			for (int i = 0; i < m_board[0].size(); i++)
				toInsert.push_back(Piece());
			m_board.insert(m_board.rbegin().base(), toInsert);
		}
		if (position.x == 0)
			for (auto &line : m_board)
				line.insert(line.begin(), Piece());
		if (position.y == 0)
		{
			std::vector<Piece> toInsert;
			for (int i = 0; i < m_board[0].size(); i++)
				toInsert.push_back(Piece());
			m_board.insert(m_board.begin(), toInsert);
		}
	}
	catch (const char *errorMessage)
	{
		std::cout << errorMessage << "\n";
	}
}
