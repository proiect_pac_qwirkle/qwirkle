#pragma once

class Board;

#include <functional>
#include <iostream>
#include "Board.h"
#include "Piece.h"

class DrawableBoard
{
public:
	DrawableBoard(Board&);
	~DrawableBoard() = default;

	std::shared_ptr<Board> GetBoardPointer();
	// print the board
	friend std::ostream& operator<<(std::ostream&, const DrawableBoard&);
private:
	std::shared_ptr<Board> m_drawableBoard;
};

