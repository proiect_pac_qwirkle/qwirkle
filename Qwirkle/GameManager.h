#pragma once
#include <string>
#include <vector>
#include "Player.h"
#include "Piece.h"
#include <random>
#include <chrono>
#include "Board.h"
#include "DrawableBoard.h"
#include "DrawablePiece.h"
#include "Keyboard.h"

class GameManager
{
public:
	Board board;
	Keyboard keyboard;
	GameManager(const std::vector<Player>&);
	enum class GameState { RUNNING, PAUSED, STOPPED };
	int getUsedPieces();
	void start();
	void pause();
	void stop();
	void fillStartingHand();
	void showRanking();
	void generateBag();
	void draw(Player&);
	void swap(Player&, Piece&);
	void swap(Player&, std::vector<int>&);
	void fillHand(Player&);
	void removePiece(std::vector<Piece>&, const int index);
	int generateRandomInt();
	Position lookForPosition();
	bool checkNeighbors(Position&);
	bool verifyOtherPieces(Position&, Piece&);
	bool checkLimits(Position&);
	enum class PlaceType {
		NONE,
		LINE,
		COLUMN
	};
	void calculateScore(Player&, std::pair<Piece, Position>&);
	int pickAnIndex();
	void showHand(Player&, DrawablePiece&);
	GameState currentState;
	void adjustPos(Position&);
	void adjustPos(Position&, Position&);
private:
	std::vector<Player> players;
	static const int totalPieces = 108;
	int usedPieces;
	std::vector<Piece> bag;
	unsigned seed = std::chrono::steady_clock::now().time_since_epoch().count();
	Keyboard::Keys key;
	Position position;
	const std::vector<int> yCoordinates = { -1, 0, 1, 0 };
	const std::vector<int> xCoordinates = { 0, 1, 0, -1 };
	bool someoneFinished = false;
	std::pair<int, int> sizeBefore;
};

