#include<iostream>

#include "GameManager.h"

int main()
{

	std::string playerName;
	std::vector<Player> players;

	std::cout << "---Enter the name of " << players.size() + 1 << "player.---";
	std::cin >> playerName;

	//enter players until "stop"
	do {

		Player player(playerName);
		players.push_back(player);

		std::cout << "---Enter the name of " << players.size() + 1 
			<< "player.---";

		std::cin >> playerName;

	} while (playerName != "stop");

	//creating a game object
	GameManager QwirkleGame(players);
	//starting the game
	QwirkleGame.start();

	return 0;
}