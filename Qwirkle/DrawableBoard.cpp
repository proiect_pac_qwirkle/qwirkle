#include "DrawableBoard.h"
#include "DrawablePiece.h"

DrawableBoard::DrawableBoard(Board &board) :
	m_drawableBoard(std::make_shared<Board>(board))
{
}

std::ostream& operator<<(std::ostream &os, const DrawableBoard &drawableBoard)
{
	for (auto line : drawableBoard.m_drawableBoard->GetBoard())
	{
		for (auto piece : line)
		{
			DrawablePiece printThis(piece);
			std::cout << printThis << " ";
		}
		std::cout << "\n";
	}
	return os;
}

std::shared_ptr<Board> DrawableBoard::GetBoardPointer()
{
	return m_drawableBoard;
}
