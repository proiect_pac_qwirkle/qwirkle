#include "DrawablePiece.h"

DrawablePiece::DrawablePiece(Piece &piece) :
	m_pieceToDraw(std::make_shared<Piece>(piece))
{
}

std::shared_ptr<Piece> DrawablePiece::GetPiecePointer()
{
	return m_pieceToDraw;
}

std::ostream& operator<<(std::ostream &os, const DrawablePiece &drawablePiece)
{
	if (drawablePiece.m_pieceToDraw->GetShape() == Piece::Shape::AnotherStar)
		os << "*";
	else if (drawablePiece.m_pieceToDraw->GetShape() == Piece::Shape::Circle)
		os << "C";
	else if (drawablePiece.m_pieceToDraw->GetShape() == Piece::Shape::Clover)
		os << "c";
	else if (drawablePiece.m_pieceToDraw->GetShape() == Piece::Shape::Diamond)
		os << "D";
	else if (drawablePiece.m_pieceToDraw->GetShape() == Piece::Shape::Square)
		os << "S";
	else if (drawablePiece.m_pieceToDraw->GetShape() == Piece::Shape::Star)
		os << "s";
	else if (drawablePiece.m_pieceToDraw->GetShape() == Piece::Shape::None)
		os << "-";
	if (drawablePiece.m_pieceToDraw->GetColor() == Piece::Color::Blue)
		os << "B";
	else if (drawablePiece.m_pieceToDraw->GetColor() == Piece::Color::Green)
		os << "G";
	else if (drawablePiece.m_pieceToDraw->GetColor() == Piece::Color::Orange)
		os << "O";
	else if (drawablePiece.m_pieceToDraw->GetColor() == Piece::Color::Purple)
		os << "P";
	else if (drawablePiece.m_pieceToDraw->GetColor() == Piece::Color::Red)
		os << "R";
	else if (drawablePiece.m_pieceToDraw->GetColor() == Piece::Color::Yellow)
		os << "Y";
	else if (drawablePiece.m_pieceToDraw->GetColor() == Piece::Color::None)
		os << "-";
	return os;
}