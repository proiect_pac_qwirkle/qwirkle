#include "Piece.h"

Piece::Piece() :
	Piece(Color::None, Shape::None)
{
	//empty
}

Piece::Piece(Color color, Shape shape) :
	m_color(color),
    m_shape(shape)
{
	//empty
}

Piece::Piece(const Piece & other)
{
	*this = other;
}

/*Piece::Piece(Piece && other)
{
	*this = std::move(other);
}*/

Piece & Piece::operator=(const Piece & other)
{
	m_color = other.m_color;
	m_shape = other.m_shape;

	return *this;
}

Piece & Piece::operator=(Piece && other)
{
	m_color = other.m_color;
	m_shape = other.m_shape;

	other.m_color = Color::None;
	other.m_shape = Shape::None;

	//new(&other) Piece;   //???

	return *this;
}

Piece::Color Piece::GetColor() const
{
	return m_color;
}

Piece::Shape Piece::GetShape() const
{
	return m_shape;
}

Piece::~Piece()
{
	m_color = Color::None;
	m_shape = Shape::None;

}
