#pragma once

#include <vector>
#include "DrawableBoard.h"
#include "Piece.h"
#include "Position.h"

class Board
{
public:
	Board();
	~Board() = default;

	// returns the board
	const std::vector<std::vector<Piece>>& GetBoard();
	// places a piece in the board and expands it if needed (uses move)
	void PlacePiece(Piece&, const Position&);
private:
	std::vector<std::vector<Piece>> m_board;
};

