#pragma once

#include <memory>
#include "Piece.h"

class DrawablePiece
{
public:
	DrawablePiece(Piece&);

	std::shared_ptr<Piece> GetPiecePointer();
	// print the piece
	friend std::ostream& operator<<(std::ostream&, const DrawablePiece&);
private:
	std::shared_ptr<Piece> m_pieceToDraw;
};

