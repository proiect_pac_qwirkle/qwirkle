#pragma once

#include<iostream>


class Piece
{
public:

	enum class Color : uint8_t
	{
		None,
		Red,
		Orange,
		Purple,
		Green,
		Blue,
		Yellow
	};

	enum class Shape : uint8_t 
	{
		None,
		Circle,
		Square,
		Star,
		AnotherStar,
		Clover,
		Diamond
	};

public:
	Piece(); //simple constructor
	Piece(Color color, Shape shape);  //constructor using fields
	Piece(const Piece& other); //copy constructor
	//Piece(Piece && other); //we we'll need constructor to move
	~Piece(); //destructor


	Piece & operator = (const Piece & other); // operator for assigning 
	Piece & operator = (Piece && other); //operator for moving


	Color GetColor() const;
	Shape GetShape() const;


private:
	Color m_color;
	Shape m_shape;
};

