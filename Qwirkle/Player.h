#pragma once
#include <string>
#include <iostream>
#include <vector>
#include "Piece.h"

class Player
{
public:
	Player();
	Player(const std::string&);
	~Player();

	enum class PlayerState { PLAYING, STANDBY, WON };

	PlayerState getPlayerState();
	void setPlayerState(PlayerState);

	std::string getPlayerName();
	void setPlayerName(const std::string&);

	void updateScore(const int);

	std::vector<Piece> hand;
	int score = 0;
private:
	PlayerState playerStatus;
	std::string name;
};

