#include "AvailablePieces.h"

#include<random>
#include<sstream>
#include<functional>

const uint8_t noOfColors = 6;
const uint8_t noOfShapes = noOfColors;


AvailablePieces::AvailablePieces()
{
	m_bag.resize(numberOfPieces);
}


double AvailablePieces::RandomFunction()
{
	/*std::random_device random;
	std::mt19937 mt(random());
	std::uniform_real_distribution<int> distribution(0, numberOfPieces-1);  //generate 108 different values

	return distribution(mt);*/

	std::function<int(int)> HashFunction = [](Piece piece)
	{
		return static_cast<int>(piece.GetShape()) * noOfColors + static_cast<int>(piece.GetColor()); 
	};

}

void AvailablePieces::GeneratePieces()
{
	for (uint8_t color = 0; color < noOfColors; ++color)
		for (uint8_t shape = 0; shape < noOfShapes; ++shape)
			EmplaceCurrentPiece(Piece(static_cast<Piece::Color>(color),
									  static_cast<Piece::Shape>(shape)));
}

void AvailablePieces::EmplaceCurrentPiece(Piece&& piece)
{
	std::stringstream stream;
	stream << piece;
	m_bag.push_back(std::forward<Piece&&>(piece));

}

void AvailablePieces::SwapPiecesFromHand(Piece && piece)
{
	Piece pickedPiece=
	m_
}

std::ostream & operator << (std::ostream & os, const AvailablePieces & availablePieces)
{
	for (const auto& iterator : availablePieces.m_bag)
		os << iterator.first << " ";
	return os;
}
