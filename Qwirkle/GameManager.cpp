#include "GameManager.h"
#include <conio.h> // _getch
#include <algorithm> // std::sort
#include <random> // modern random


GameManager::GameManager(const std::vector<Player>& players) {
	this->players = players;
	usedPieces = 0;
	currentState = GameState::STOPPED;
}

void GameManager::start() {
	currentState = GameState::RUNNING;
	DrawableBoard drawBoard(board);
	Piece nullPiece;
	DrawablePiece drawPiece(nullPiece);
	generateBag();
	fillStartingHand();
	std::get<0>(sizeBefore) = board.GetBoard().size();
	std::get<1>(sizeBefore) = board.GetBoard()[0].size();
	while (currentState != GameState::STOPPED) {
		for (Player& currentPlayer : players) {
			// executes player's turn
			currentPlayer.setPlayerState(Player::PlayerState::PLAYING);
			int handIndex;
			std::pair<Piece, Position> firstPiece;
			Piece currentPiece;
			std::pair<Piece, Position> previousPiece;
			std::get<1>(previousPiece).y = -1;
			std::get<1>(previousPiece).x = -1;
			PlaceType rule = GameManager::PlaceType::NONE;
			drawBoard = DrawableBoard(board);
			std::cout << drawBoard;
			std::cout << currentPlayer.getPlayerName() << "'s turn. Please proceed on making your move.\n";
			showHand(currentPlayer, drawPiece);
			std::cout << "Press R to swap pieces. Press anything else to begin your turn.\n";
			key = keyboard.GetKeyPress();
			//swap phase
			if (key == Keyboard::Keys::R) {
				// NOTE: replace 'true' with logical statement based on wether the current player wants to swap or not
				std::cout << "Specify the indexes from the pieces you wish to swap, from 0 to 5, 1 by 1.\n";
				std::vector<int> toSwap;
				do {
					std::cin >> handIndex;
					toSwap.push_back(handIndex);
					std::cout << "Press R to insert another index to swap, press anything else to continue the game.\n";
					key = keyboard.GetKeyPress();
				} while (key == Keyboard::Keys::R);
				swap(currentPlayer, toSwap);
				showHand(currentPlayer, drawPiece);
				// NOTE: replace true to end the swap phase
			}
			else {
				// pick phase
				while (key != Keyboard::Keys::X) {
					//first move of the game
					if (board.GetBoard().size() == 1) {
						handIndex = pickAnIndex();
						currentPiece = currentPlayer.hand[handIndex];
						board.PlacePiece(currentPlayer.hand[handIndex], Position(0, 0));
						removePiece(currentPlayer.hand, handIndex);
						drawBoard = DrawableBoard(board);
						std::cout << drawBoard;
						showHand(currentPlayer, drawPiece);
						std::get<0>(previousPiece) = currentPiece;
						std::get<1>(previousPiece) = Position(0, 0);
						firstPiece = previousPiece;
						adjustPos(std::get<1>(firstPiece)); 
						std::get<0>(sizeBefore) = board.GetBoard().size();
						std::get<1>(sizeBefore) = board.GetBoard()[0].size();
						std::cout << "Press X if you want to end your turn. Press any other key to continue.\n";
						key = keyboard.GetKeyPress();

						if (key == Keyboard::Keys::X)
							break;
					}
					// place phase
					try {
						handIndex = pickAnIndex();
						currentPiece = currentPlayer.hand[handIndex];
						position = lookForPosition();
						if (board.GetBoard()[position.y][position.x].GetColor() != Piece::Color::None)
							throw "Invalid move! Pick an empty spot.";
						if (!checkNeighbors(position))
							throw "Invalid move! The piece has to be places next to another piece.";
						if (!verifyOtherPieces(position, currentPiece))
							throw "Invalid move! The chosen piece needs to have matching color/shape as all other adjacent lines.";
						if (rule != GameManager::PlaceType::NONE) {
							if (rule == GameManager::PlaceType::LINE && position.y != std::get<1>(previousPiece).y)
								throw "Invalid move! Pieces you place have to form a line.";
							else if (rule == GameManager::PlaceType::COLUMN && position.x != std::get<1>(previousPiece).x)
								throw "Invalid move! Pieces you place have to form a line.";
						}
						board.PlacePiece(currentPlayer.hand[handIndex], position);
						if (std::get<1>(previousPiece).y == -1){
							std::get<0>(firstPiece) = currentPiece;
							std::get<1>(firstPiece) = position;
							adjustPos(std::get<1>(firstPiece));
							std::get<0>(sizeBefore) = board.GetBoard().size();
							std::get<1>(sizeBefore) = board.GetBoard()[0].size();
						}
						else {
							adjustPos(position);
							adjustPos(std::get<1>(firstPiece), position);
							std::get<0>(sizeBefore) = board.GetBoard().size();
							std::get<1>(sizeBefore) = board.GetBoard()[0].size();
						}
						removePiece(currentPlayer.hand, handIndex);
						drawBoard = DrawableBoard(board);
						std::cout << drawBoard;
						showHand(currentPlayer, drawPiece);
						if (std::get<1>(previousPiece).y != -1) {
							if (std::get<1>(previousPiece).y == position.y) {
								rule = GameManager::PlaceType::LINE;
							}
							else {
								rule = GameManager::PlaceType::COLUMN;
							}
						}
						std::get<0>(previousPiece) = currentPiece;
						std::get<1>(previousPiece) = position;
					}
					catch (const char *errorMessage) {
						std::cout << errorMessage << "\n";
					}

					if (currentPlayer.hand.empty() && !bag.empty()) {
						currentPlayer.updateScore(6);
					}
					if (currentPlayer.hand.empty() && bag.empty() && someoneFinished == false) {
						someoneFinished = true;
						currentPlayer.updateScore(6);
					}
					std::cout << "Press X to end your turn, press any other key to continue\n";
					key = keyboard.GetKeyPress();
				}

				calculateScore(currentPlayer, firstPiece);
				fillHand(currentPlayer);
			}
			currentPlayer.setPlayerState(Player::PlayerState::STANDBY);
			if (usedPieces == totalPieces) {
				// stops the game if the number of played pieces matches the total number of pieces
				stop();
				showRanking();
				break;
			}
		}
	}
}

void GameManager::pause() {
	// freezes the game and switches to PAUSED state until a text is inserted
	currentState = GameState::PAUSED;
	std::cout << "Game paused! Insert any value to continue...\n";
	_getch();
	currentState = GameState::RUNNING;
}

void GameManager::stop() {
	// marks the end of the game by switching to STOPPED state
	currentState = GameState::STOPPED;
}

void GameManager::generateBag() {
	for (uint8_t colorIterator = 1; colorIterator < 7; ++colorIterator)
		for (uint8_t shapeIterator = 1; shapeIterator < 7; ++shapeIterator) {
			Piece newPiece(static_cast<Piece::Color>(colorIterator), static_cast<Piece::Shape>(shapeIterator));
			for (int counter = 0; counter < 3; ++counter)
				bag.push_back(newPiece);
		}

}

void GameManager::fillStartingHand() {
	// populates each player's hand with 6 tiles from the bag
	for (Player& currentPlayer : players) {
		fillHand(currentPlayer);
		usedPieces += 6;
	}
}

bool comparePlayers(const Player& first, const Player& second) {
	// sort container by score function
	return first.score < second.score;
}

void GameManager::showRanking() {
	// sorts the players by score and returns a new vector after the sort
	std::vector<Player> ranking = this->players;
	std::sort(ranking.begin(), ranking.end(), comparePlayers);
	ranking[0].setPlayerState(Player::PlayerState::WON);
	for (int iterator = 0; iterator < ranking.size(); ++iterator) {
		std::cout << iterator + 1 << ". " << ranking[0].getPlayerName() << " scoring a total of " << ranking[0].score << " points.\n";
	}
}

void GameManager::draw(Player& player) {
	// picks a random tile from the bag and adds it to this player's hand
	int randomIndex = generateRandomInt();
	player.hand.push_back(bag[randomIndex]);
	removePiece(bag, randomIndex);
}

void GameManager::swap(Player& player, Piece& toSwap) {
	// picks a random piece from the bag and swaps it with a chosen piece from the player's hand
	int randomIndex = generateRandomInt();
	std::swap(toSwap, bag[randomIndex]);
}

void GameManager::swap(Player& player, std::vector<int>& toSwap) {
	// overloaded function to take an array of indexes instead of a single piece
	for (int index : toSwap)
		swap(player, player.hand[index]);
}

void GameManager::fillHand(Player& player) {
	// function called when a player has less than 6 tiles in their hand
	while (player.hand.size() < 6 && !bag.empty())
		draw(player);
}

void GameManager::removePiece(std::vector<Piece>& from, int position) {
	// removes the piece which is placed at a certain position by swapping it with the last element and then pops the last element of the vector
	std::swap(from[position], from.back());
	from.pop_back();
}

int GameManager::generateRandomInt() {
	std::default_random_engine generator(seed);
	std::uniform_int_distribution<int> distribution(0, bag.size() - 1);
	int randomInt = distribution(generator);
	return randomInt;
}

Position GameManager::lookForPosition() {
	// goes through the matrix using keyboard inputs and returns a certain position
	std::cout << "Use W, A, S, D keys to pick the desired position: ";
	Position pos;
	pos.y = 0;
	pos.x = 0;
	key = keyboard.GetKeyPress();
	while (key != Keyboard::Keys::ENTER) {
		if ((key == Keyboard::Keys::ARROW_LEFT || key == Keyboard::Keys::A) && pos.x > 0) {
			pos.x--;
		}
		else if ((key == Keyboard::Keys::ARROW_RIGHT || key == Keyboard::Keys::D) && pos.x < board.GetBoard()[0].size() - 1) {
			pos.x++;
		}
		else if ((key == Keyboard::Keys::ARROW_UP || key == Keyboard::Keys::W) && pos.y > 0) {
			pos.y--;
		}
		else if ((key == Keyboard::Keys::ARROW_DOWN || key == Keyboard::Keys::S) && pos.y < board.GetBoard().size() - 1) {
			pos.y++;
		}
		std::cout << pos.y << " " << pos.x << " ";
		key = keyboard.GetKeyPress();
	}
	std::cout << "\n";
	return pos;
}

bool GameManager::checkNeighbors(Position& position) {
	// checks if the current position got any neighboring pieces
	Position currentPosition;
	for (int iterator = 0; iterator < 4; ++iterator) {
		currentPosition.y = position.y + yCoordinates[iterator];
		currentPosition.x = position.x + xCoordinates[iterator];
		if (checkLimits(currentPosition))
			if (board.GetBoard()[currentPosition.y][currentPosition.x].GetColor() != Piece::Color::None)
				return true;
	}
	return false;
}

bool GameManager::verifyOtherPieces(Position& position, Piece& piece) {
	// takes all arrays of pieces that start from this position and returns false if the piece that needs to be placed is found in any
	// of those arrays or if the piece has no matching color/shape as the array; returns true otherwise
	Position currentPosition;
	Piece currentPiece;
	bool colorCriteria;
	bool shapeCriteria;
	for (int iterator = 0; iterator < 4; ++iterator) {
		currentPosition.y = position.y + yCoordinates[iterator];
		currentPosition.x = position.x + xCoordinates[iterator];
		colorCriteria = true;
		shapeCriteria = true;
		if (checkLimits(currentPosition)) {
			currentPiece = board.GetBoard()[currentPosition.y][currentPosition.x];
			while (currentPiece.GetColor() != Piece::Color::None && checkLimits(currentPosition)) {
				if (currentPiece.GetColor() == piece.GetColor() && currentPiece.GetShape() == piece.GetShape())
					return false;
				if (currentPiece.GetColor() != piece.GetColor())
					colorCriteria = false;
				if (currentPiece.GetShape() != piece.GetShape())
					shapeCriteria = false;
				if (colorCriteria == false && shapeCriteria == false)
					return false;
				currentPosition.y += yCoordinates[iterator];
				currentPosition.x += xCoordinates[iterator];
				currentPiece = board.GetBoard()[currentPosition.y][currentPosition.x];
			}
		}
	}
	return true;
}

bool GameManager::checkLimits(Position& position) {
	// checks if a gives position is within the board limits
	if (position.y < 0 || position.y >= board.GetBoard().size())
		return false;
	if (position.x < 0 || position.x >= board.GetBoard()[0].size())
		return false;
	return true;
}

void GameManager::calculateScore(Player& player, std::pair<Piece, Position>& startingPiece) {
	// calculates the points that this player has scored in its turn by counting the pieces connected through lines to the starting piece
	int horizontalLine = -1;
	int verticalLine = -1;
	std::pair<Piece, Position> currentPiece = startingPiece;
	int overallScore = 0;
	// calculates the score on the vertical line
	while (std::get<0>(currentPiece).GetColor() != Piece::Color::None) {
		horizontalLine++;
		std::get<1>(currentPiece).x--;
		if (!checkLimits(std::get<1>(currentPiece))) {
			break;
		}
		std::get<0>(currentPiece) = board.GetBoard()[std::get<1>(currentPiece).y][std::get<1>(currentPiece).x];
	}
	
	currentPiece = startingPiece;
	while (std::get<0>(currentPiece).GetColor() != Piece::Color::None) {
		horizontalLine++;
		std::get<1>(currentPiece).x++;
		if (!checkLimits(std::get<1>(currentPiece))) {
			break;
		}
		std::get<0>(currentPiece) = board.GetBoard()[std::get<1>(currentPiece).y][std::get<1>(currentPiece).x];
	}
	// calculates the score on the horizontal line
	currentPiece = startingPiece;
	while (std::get<0>(currentPiece).GetColor() != Piece::Color::None) {
		verticalLine++;
		std::get<1>(currentPiece).y--;
		if (!checkLimits(std::get<1>(currentPiece))) {
			break;
		}
		std::get<0>(currentPiece) = board.GetBoard()[std::get<1>(currentPiece).y][std::get<1>(currentPiece).x];
	}
	currentPiece = startingPiece;
	while (std::get<0>(currentPiece).GetColor() != Piece::Color::None) {
		verticalLine++;
		std::get<1>(currentPiece).y++;
		if (!checkLimits(std::get<1>(currentPiece))) {
			break;
		}
		std::get<0>(currentPiece) = board.GetBoard()[std::get<1>(currentPiece).y][std::get<1>(currentPiece).x];
	}
	if (verticalLine == -1 || verticalLine == 1)
		verticalLine = 0;
	if (horizontalLine == -1 || horizontalLine == 1)
		horizontalLine = 0;
	if (verticalLine == 6)
		overallScore += 6;
	if (horizontalLine == 6)
		overallScore += 6;
	overallScore += (verticalLine + horizontalLine);
	player.updateScore(overallScore);
	std::cout << "Player " << player.getPlayerName() << " has scored a total of " << overallScore << " points this turn! Total score: " << player.score << ".\n";
}

int GameManager::pickAnIndex() {
	std::cout << "Use A and D keys to pick the desired piece index: ";
	int handIndex = 0;
	key = keyboard.GetKeyPress();
	while (key != Keyboard::Keys::ENTER) {
		if ((key == Keyboard::Keys::ARROW_LEFT || key == Keyboard::Keys::A) && handIndex > 0) {
			handIndex--;
		}
		if ((key == Keyboard::Keys::ARROW_RIGHT || key == Keyboard::Keys::D) && handIndex < 5) {
			handIndex++;
		}
		std::cout << handIndex << " ";
		key = keyboard.GetKeyPress();
	}
	std::cout << "\n";
	return handIndex;
}

void GameManager::showHand(Player& player, DrawablePiece& drawPiece) {
	for (Piece piece : player.hand) {
		drawPiece = DrawablePiece(piece);
		std::cout << drawPiece << " ";
	}
	std::cout << "\n";
}

int GameManager::getUsedPieces() {
	return usedPieces;
}

void GameManager::adjustPos(Position& position) {
	// adjust a certain position to match the resized board
	if ((std::get<0>(sizeBefore) < board.GetBoard().size() && std::get<1>(sizeBefore) < board.GetBoard()[0].size()) && (position.y == 0 && position.x == 0)) {
		position.y++;
		position.x++;
	}
	else if (std::get<0>(sizeBefore) < board.GetBoard().size() && position.y == 0) {
		position.y++;
	}
	else if (std::get<1>(sizeBefore) < board.GetBoard()[0].size() && position.x == 0) {
		position.x++;
	}
}

void GameManager::adjustPos(Position& position, Position& nextPos) {
	// adjust a certain position based on the next placed piece'
	if (std::get<0>(sizeBefore) < board.GetBoard().size()) {
		if (position.y == nextPos.y - 1) {
			position.y++;
		}
	}
	else if (std::get<1>(sizeBefore) < board.GetBoard()[0].size()) {
		if (position.x == nextPos.x - 1) {
			position.x++;
		}
	}
}