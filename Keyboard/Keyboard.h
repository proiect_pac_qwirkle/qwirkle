#pragma once

#ifdef KEYBOARD_EXPORTS  
#define KEYBOARD_API __declspec(dllexport)   
#else  
#define KEYBOARD_API __declspec(dllimport)   
#endif 

class KEYBOARD_API Keyboard 
{
public:
	enum Keys {
		ARROW_UP,
		ARROW_DOWN,
		ARROW_LEFT,
		ARROW_RIGHT,
		ENTER,
		W,
		A,
		S,
		D,
		R,
		P,
		X,
		C
	};

	// waits for a key to pressed and returns it
	Keys GetKeyPress();
};