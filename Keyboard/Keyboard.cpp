#include "Keyboard.h"
#include <conio.h>

Keyboard::Keys Keyboard::GetKeyPress()
{
	char pressedButton = _getch();
	if (pressedButton == 'w' || pressedButton == 'W')
		return Keyboard::Keys::W;
	if (pressedButton == 'a' || pressedButton == 'A')
		return Keyboard::Keys::A;
	if (pressedButton == 's' || pressedButton == 'S')
		return Keyboard::Keys::S;
	if (pressedButton == 'd' || pressedButton == 'D')
		return Keyboard::Keys::D;
	if (pressedButton == 'r' || pressedButton == 'R')
		return Keyboard::Keys::R;
	if (pressedButton == 'p' || pressedButton == 'P')
		return Keyboard::Keys::P;
	if (pressedButton == 'x' || pressedButton == 'X')
		return Keyboard::Keys::X;
	if (pressedButton == 'c' || pressedButton == 'C')
		return Keyboard::Keys::C;
	if (pressedButton == '\r')
		return Keyboard::Keys::ENTER;
	if (pressedButton == 0 || pressedButton == 224) 
	{
		pressedButton = _getch();
		if (pressedButton == 72)
			return Keyboard::Keys::ARROW_UP;
		if (pressedButton == 80)
			return Keyboard::Keys::ARROW_DOWN;
		if (pressedButton == 75)
			return Keyboard::Keys::ARROW_LEFT;
		if (pressedButton == 77)
			return Keyboard::Keys::ARROW_RIGHT;
	}
}
